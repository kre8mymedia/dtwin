# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

Clone the repo and then run `npm install`.

### Docker

Run the following command docker network.

```js
docker-compose up --build
```

### Migrations

Run the following command to run startup migrations.

```js
yarn docker:dev:cmd adonis migration:run
```
