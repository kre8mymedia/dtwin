require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');
const { Client } = require('@elastic/elasticsearch');

const redisObj = {
  url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
}
const redisClient = redis.createClient(redisObj);
const mqttClient = mqtt.connect(process.env.MQTT_PROTOCOL + process.env.MQTT_URL || "104.15.248.11", {
  host: process.env.MQTT_URL || "104.15.248.11",
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: (process.env.MQTT_CERT_VERIFY === 'true')
});
const elasticClient = new Client({
  node: process.env.ELASTIC_URL
})

/**
 * Topic subscribed too
 */

const topic = 'gps/#'
console.log('Topic: ', topic)

mqttClient.subscribe(topic)

mqttClient.on('message', function (topic, message) {
  let topic_str = topic.toString()
  // split the topic str into array
  topic_str = topic_str.split('/')

  console.log(topic)

  const payload = JSON.parse(message)
  payload['device_id'] = topic_str[1]
  payload['timestamp'] = (new Date).toISOString()
  // console.log(payload)

  redisClient.setex(`${topic_str[0]}:${topic_str[1]}`, 15000, JSON.stringify(payload), redis.print);

  async function run() {

    await elasticClient.indices.create({
      index: 'gps-metrics',
      body: {
        mappings: {
          properties: {
            ip: { type: 'text', doc_values: true },
            hostname: { type: 'text', doc_values: true },
            city: { type: 'text', doc_values: true },
            region: { type: 'text', doc_values: true },
            country: { type: 'text', doc_values: true },
            loc: { type: 'text', doc_values: true },
            org: { type: 'text', doc_values: true },
            postal: { type: 'text', doc_values: true },
            timezone: { type: 'text', doc_values: true },
            timestamp: { type: 'date', doc_values: true }
          }
        }
      }
    }, { ignore: [400] })

    const dataset = [payload];

    const body = dataset.flatMap(doc => [{ index: { _index: 'gps-metrics' } }, doc])

    const { body: bulkResponse } = await elasticClient.bulk({ refresh: true, body })

    if (bulkResponse.errors) {
      const erroredDocuments = []
      // The items array has the same order of the dataset we just indexed.
      // The presence of the `error` key indicates that the operation
      // that we did for the document has failed.
      bulkResponse.items.forEach((action, i) => {
        const operation = Object.keys(action)[0]
        if (action[operation].error) {
          erroredDocuments.push({
            // If the status is 429 it means that you can retry the document,
            // otherwise it's very likely a mapping error, and you should
            // fix the document before to try it again.
            status: action[operation].status,
            error: action[operation].error,
            operation: body[i * 2],
            document: Object.values(body[i * 2 + 1].value)
          })
        }
      })
      console.log(erroredDocuments)
    }

    const { body: count } = await elasticClient.count({ index: 'gps-metrics' })
    console.log({
      data_type: 'gps',
      timestamp: (new Date).toISOString(),
      datetime: (new Date).toLocaleString('en-US', { timeZone: 'America/Chicago' }),
      elastic: count
    })
  }
  if (process.env.ELASTIC_URL) {
    // run().catch(console.log);
  }
})

console.log(`gps.js started successfully ${(new Date).toISOString()}`)
