require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');
const { Client } = require('@elastic/elasticsearch');

const redisObj = {
  url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
}
const redisClient = redis.createClient(redisObj);
const mqttClient = mqtt.connect(process.env.MQTT_PROTOCOL + process.env.MQTT_URL || "104.15.248.11", {
  host: process.env.MQTT_URL || "104.15.248.11",
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: (process.env.MQTT_CERT_VERIFY === 'true')
});
const elasticClient = new Client({
  node: process.env.ELASTIC_URL
})

/**
 * Topic subscribed too
 */

const topic = 'response/#'
console.log('Topic: ', topic)

mqttClient.subscribe(topic)

mqttClient.on('message', function (topic, message) {
  let topic_str = topic.toString()
  // split the topic str into array
  topic_str = topic_str.split('/')
  

  // console.log(cpu_stats)

  redisClient.setex(`${topic_str[0]}:${topic_str[1]}:${topic_str[2]}`, 15000, message, redis.print);

  // async function run() {

  //   await elasticClient.indices.create({
  //     index: 'device-metrics',
  //     body: {
  //       mappings: {
  //         properties: {
  //           timestamp: { type: 'date', doc_values: true },
  //           cpu: { type: 'flattened', doc_values: true },
  //           cpu_0: { type: 'flattened', doc_values: true },
  //           cpu_1: { type: 'flattened', doc_values: true },
  //           cpu_2: { type: 'flattened', doc_values: true },
  //           cpu_3: { type: 'flattened', doc_values: true },
  //           ctxt: { type: 'integer', doc_values: true },
  //           processes: { type: 'integer', doc_values: true },
  //           procs_running: { type: 'integer', doc_values: true },
  //         }
  //       }
  //     }
  //   }, { ignore: [400] })

  //   const dataset = [cpu_stats];

  //   const body = dataset.flatMap(doc => [{ index: { _index: 'device-metrics' } }, doc])

  //   const { body: bulkResponse } = await elasticClient.bulk({ refresh: true, body })

  //   if (bulkResponse.errors) {
  //     const erroredDocuments = []
  //     // The items array has the same order of the dataset we just indexed.
  //     // The presence of the `error` key indicates that the operation
  //     // that we did for the document has failed.
  //     bulkResponse.items.forEach((action, i) => {
  //       const operation = Object.keys(action)[0]
  //       if (action[operation].error) {
  //         erroredDocuments.push({
  //           // If the status is 429 it means that you can retry the document,
  //           // otherwise it's very likely a mapping error, and you should
  //           // fix the document before to try it again.
  //           status: action[operation].status,
  //           error: action[operation].error,
  //           operation: body[i * 2],
  //           document: Object.values(body[i * 2 + 1].value)
  //         })
  //       }
  //     })
  //     console.log(erroredDocuments)
  //   }

  //   const { body: count } = await elasticClient.count({ index: 'device-metrics' })
  //   console.log({
  //     data_type: 'cpu',
  //     timestamp: (new Date).toISOString(),
  //     datetime: (new Date).toLocaleString('en-US', { timeZone: 'America/Chicago' }),
  //     elastic: count
  //   })
  // }
  // if (process.env.ELASTIC_URL) {
  //   // run().catch(console.log);
  // }
})

console.log(`response.js started successfully ${(new Date).toISOString()}`)
