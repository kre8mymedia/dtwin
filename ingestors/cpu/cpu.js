require('dotenv').config()
const mqtt = require('mqtt')
const redis = require('redis');
const { Client } = require('@elastic/elasticsearch');

const redisObj = {
  url: process.env.REDIS_URL || process.env.REDIS_PROTOCOL + process.env.REDIS_PASSWORD + "@" + process.env.REDIS_HOST + ":" + process.env.REDIS_PORT
}
const redisClient = redis.createClient(redisObj);
const mqttClient = mqtt.connect(process.env.MQTT_PROTOCOL + process.env.MQTT_URL || "104.15.248.11", {
  host: process.env.MQTT_URL || "104.15.248.11",
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: (process.env.MQTT_CERT_VERIFY === 'true')
});
const elasticClient = new Client({
  node: process.env.ELASTIC_URL
})

/**
 * Topic subscribed too
 */

const topic = 'cpu_stats/#'
console.log('Topic: ', topic)

mqttClient.subscribe(topic)

mqttClient.on('message', function (topic, message) {
  let topic_str = topic.toString()
  // split the topic str into array
  topic_str = topic_str.split('/')
  console.log(topic)
  
  const stats = message.toString().split("\n")
  stats.pop()
  stats.splice(5, 1)
  
  const cpu_total_row = stats[0]
  const cpu_total = {
    user: parseInt(cpu_total_row.split(" ")[2]),
    nice: parseInt(cpu_total_row.split(" ")[3]),
    system: parseInt(cpu_total_row.split(" ")[4]),
    idle: parseInt(cpu_total_row.split(" ")[5]),
    iowait: parseInt(cpu_total_row.split(" ")[6]),
    irq: parseInt(cpu_total_row.split(" ")[7]),
    softirq: parseInt(cpu_total_row.split(" ")[8])
  }

  const cpu_row_0 = stats[1]
  const cpu_0 = {
    user: parseInt(cpu_row_0.split(" ")[2]),
    nice: parseInt(cpu_row_0.split(" ")[3]),
    system: parseInt(cpu_row_0.split(" ")[4]),
    idle: parseInt(cpu_row_0.split(" ")[5]),
    iowait: parseInt(cpu_row_0.split(" ")[6]),
    irq: parseInt(cpu_row_0.split(" ")[7]),
    softirq: parseInt(cpu_row_0.split(" ")[8])
  }

  const cpu_row_1 = stats[2]
  const cpu_1 = {
    user: parseInt(cpu_row_1.split(" ")[2]),
    nice: parseInt(cpu_row_1.split(" ")[3]),
    system: parseInt(cpu_row_1.split(" ")[4]),
    idle: parseInt(cpu_row_1.split(" ")[5]),
    iowait: parseInt(cpu_row_1.split(" ")[6]),
    irq: parseInt(cpu_row_1.split(" ")[7]),
    softirq: parseInt(cpu_row_1.split(" ")[8])
  }

  const cpu_row_2 = stats[3]
  const cpu_2 = {
    user: parseInt(cpu_row_2.split(" ")[2]),
    nice: parseInt(cpu_row_2.split(" ")[3]),
    system: parseInt(cpu_row_2.split(" ")[4]),
    idle: parseInt(cpu_row_2.split(" ")[5]),
    iowait: parseInt(cpu_row_2.split(" ")[6]),
    irq: parseInt(cpu_row_2.split(" ")[7]),
    softirq: parseInt(cpu_row_2.split(" ")[8])
  }

  const cpu_row_3 = stats[4]
  const cpu_3 = {
    user: parseInt(cpu_row_3.split(" ")[2]),
    nice: parseInt(cpu_row_3.split(" ")[3]),
    system: parseInt(cpu_row_3.split(" ")[4]),
    idle: parseInt(cpu_row_3.split(" ")[5]),
    iowait: parseInt(cpu_row_3.split(" ")[6]),
    irq: parseInt(cpu_row_3.split(" ")[7]),
    softirq: parseInt(cpu_row_3.split(" ")[8])
  }

  const ctxt_row = stats[5]
  const processes_row = stats[7]
  const procs_running_row = stats[8]

  const cpu_stats = {}
  cpu_stats['device_id'] = topic_str[1]
  cpu_stats['timestamp'] = (new Date).toISOString()
  cpu_stats[cpu_total_row.split(" ")[0]] = cpu_total
  cpu_stats[cpu_row_0.split(" ")[0]] = cpu_0
  cpu_stats[cpu_row_1.split(" ")[0]] = cpu_1
  cpu_stats[cpu_row_2.split(" ")[0]] = cpu_2
  cpu_stats[cpu_row_3.split(" ")[0]] = cpu_3
  cpu_stats[ctxt_row.split(" ")[0]] = parseInt(ctxt_row.split(" ")[1])
  cpu_stats[processes_row.split(" ")[0]] = parseInt(processes_row.split(" ")[1])
  cpu_stats[procs_running_row.split(" ")[0]] = parseInt(procs_running_row.split(" ")[1])

  // console.log(cpu_stats)

  redisClient.setex(`${topic_str[0]}:${topic_str[1]}`, 15000, JSON.stringify(cpu_stats), redis.print);

  async function run() {

    await elasticClient.indices.create({
      index: 'device-metrics',
      body: {
        mappings: {
          properties: {
            timestamp: { type: 'date', doc_values: true },
            cpu: { type: 'flattened', doc_values: true },
            cpu_0: { type: 'flattened', doc_values: true },
            cpu_1: { type: 'flattened', doc_values: true },
            cpu_2: { type: 'flattened', doc_values: true },
            cpu_3: { type: 'flattened', doc_values: true },
            ctxt: { type: 'integer', doc_values: true },
            processes: { type: 'integer', doc_values: true },
            procs_running: { type: 'integer', doc_values: true },
          }
        }
      }
    }, { ignore: [400] })

    const dataset = [cpu_stats];

    const body = dataset.flatMap(doc => [{ index: { _index: 'device-metrics' } }, doc])

    const { body: bulkResponse } = await elasticClient.bulk({ refresh: true, body })

    if (bulkResponse.errors) {
      const erroredDocuments = []
      // The items array has the same order of the dataset we just indexed.
      // The presence of the `error` key indicates that the operation
      // that we did for the document has failed.
      bulkResponse.items.forEach((action, i) => {
        const operation = Object.keys(action)[0]
        if (action[operation].error) {
          erroredDocuments.push({
            // If the status is 429 it means that you can retry the document,
            // otherwise it's very likely a mapping error, and you should
            // fix the document before to try it again.
            status: action[operation].status,
            error: action[operation].error,
            operation: body[i * 2],
            document: Object.values(body[i * 2 + 1].value)
          })
        }
      })
      console.log(erroredDocuments)
    }

    const { body: count } = await elasticClient.count({ index: 'device-metrics' })
    console.log({
      data_type: 'cpu',
      timestamp: (new Date).toISOString(),
      datetime: (new Date).toLocaleString('en-US', { timeZone: 'America/Chicago' }),
      elastic: count
    })
  }
  if (process.env.ELASTIC_URL) {
    // run().catch(console.log);
  }
})

console.log(`cpu.js started successfully ${(new Date).toISOString()}`)
