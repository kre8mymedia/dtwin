#!/bin/bash

echo "Please enter your Device ID:"
echo "If you haven't created one yet make one here -> http://localhost:3333/api/v1/devices"
read device_id
echo ""
echo "Device ID: $device_id"
echo $device_id > ~/device-info/id.txt

echo ">> Creating Directories..."
mkdir ~/sia
mkdir ~/sia/videos
mkdir ~/device-info
mkdir /tmp/siasky

echo ">> Installing JQ..."
apt-get install -y jq

echo ">> Installing motion..."
apt-get install -y motion
echo "============================"
echo " Motion.Conf "
echo "============================"
sed -i 's#target_dir /var/lib/motion#target_dir /tmp/siasky#g' /etc/motion/motion.conf
sed -i 's/movie_codec mkv/movie_codec mp4/' /etc/motion/motion.conf
cat /etc/motion/motion.conf

echo ""
echo ""
echo ""
echo ""
echo "Install Ngrok -> https://ngrok.com/"