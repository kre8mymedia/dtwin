#!/bin/bash

FILES=$(ls /videos/*.mp4)

for f in $FILES
do
  echo "$f" | (read foo; curl -L -X POST "https://siasky.net/skynet/skyfile" -F "file=@$foo" | echo "$(date +%s) - https://siasky.net/$(jq -r '.skylink')" >> /tmp/siasky/info.txt)
  rm $f
done