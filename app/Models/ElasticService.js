'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

const { Client } = require('@elastic/elasticsearch')

const elasticClient = new Client({
  node: process.env.ELASTIC_URL
})

class ElasticService extends Model {

  static insertAuditRecord(method, user_id, device_id, action) {
    
    const entry = elasticClient.index({
      index: 'audit-logs',
      body: {
        timestamp: new Date().getTime(),
        device_id: device_id,
        user_id: user_id,
        method: method,
        action: action
      }
    })
    return entry
  }

}

module.exports = ElasticService
