'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Organization extends Model {

  static boot() {
    super.boot();
    this.addHook("beforeCreate", "OrganizationHook.uuid");
  }

  static get primaryKey() {
    return "id";
  }

  static get incrementing() {
    return false;
  }

  users() {
    return this.hasMany('App/Models/User')
  }

  devices() {
    return this.hasMany('App/Models/Device')
  }

}

module.exports = Organization
