'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Role extends Model {

  static boot() {
    super.boot();
    this.addHook("beforeCreate", "RoleHook.uuid");
  }

  static get primaryKey() {
    return "id";
  }

  static get incrementing() {
    return false;
  }

  organization() {
    return this.belongsTo('App/Models/Organization')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

}

module.exports = Role
