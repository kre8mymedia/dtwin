'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Model = use('Model')
const Redis = use('Redis')

class Device extends Model {

  static boot() {
    super.boot();
    this.addHook("beforeCreate", "DeviceHook.uuid");
  }
  
  static get primaryKey() {
    return "id";
  }

  static get incrementing() {
    return false;
  }

  static get hidden () {
    return ['password', 'location.device_id', 'info.device_id']
  }

  organization() {
    return this.belongsTo('App/Models/Organization')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

}

module.exports = Device
