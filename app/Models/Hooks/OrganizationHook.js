'use strict'

const { v4: uuidv4 } = require('uuid');

const OrganizationHook = exports = module.exports = {}

OrganizationHook.uuid = async organization => {
  organization.id = uuidv4()
}
