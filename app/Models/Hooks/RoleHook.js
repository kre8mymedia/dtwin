'use strict'

const { v4: uuidv4 } = require('uuid');

const RoleHook = exports = module.exports = {}

RoleHook.uuid = async role => {
  role.id = uuidv4()
}
