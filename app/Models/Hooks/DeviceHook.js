'use strict'

const { v4: uuidv4 } = require('uuid');
const Redis = use('Redis')

const DeviceHook = exports = module.exports = {}

DeviceHook.uuid = async device => {
  device.id = uuidv4()
}

DeviceHook.info = async device => {
  device.info = await Redis.get(`cpu_stats:${device.id}`)
}