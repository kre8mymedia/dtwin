'use strict'

// Models
const Device = use('App/Models/Device')
const Role = use('App/Models/Role')
const ElasticService = use('App/Models/ElasticService')

const Redis = use('Redis')
const { Client } = require('@elastic/elasticsearch')
const mqtt = require('mqtt')

const { validate } = use('Validator')

const elasticClient = new Client({
  node: process.env.ELASTIC_URL
})

const mqttClient = mqtt.connect(process.env.MQTT_PROTOCOL + process.env.MQTT_URL || "104.15.248.11", {
  host: process.env.MQTT_URL || "104.15.248.11",
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  rejectUnauthorized: (process.env.MQTT_CERT_VERIFY === 'true')
});

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with devices
 */
class DeviceController {
  /**
   * Show a list of all devices.
   * GET devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ auth, request, response, view }) {
    const role = await Role.query().where({
      device_index: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role && auth.user.organization_id) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }
                          
    const devices_with_user = await Device.query()
                                .where('user_id', auth.user.id)
                                .with('user')
                                .fetch()

    const devices_with_org = await Device.query()
                                .where('organization_id', auth.user.organization_id)
                                .with('organization')
                                .fetch()
    
    const devices = (auth.user.organization_id) ? devices_with_org : devices_with_user

    const res = {
      succes: true,
      devices: devices
    }

    if (auth.user.organization_id) {
      res['my_devices'] = devices_with_user
    }

    // AUDIT RECORD
    // await ElasticService.insertAuditRecord(auth.user.id, `Listing all units that belong to user at ${auth.user.id}`)

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return view.render('devices.index', {
        posts: res.toJSON()
      })
    }
  }

  /**
   * Create/save a new device.
   * POST devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const role = await Role.query().where({
      device_store: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role && auth.user.organization_id) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    // Validate
    const validation = await validate(request.all(), {
      slug: 'required|string|min:2|max:255'
    })
    // Check if validation failed
    if(validation.fails()) {
      return response.json(validation.messages())
    }

    // Create new instance
    const device = new Device();
    // Assign detials
    device.slug = request.input('slug')
    if (request.input('type') == 'org') {
      device.organization_id = auth.user.organization_id
    } else if(request.input('type') == 'user') {
      device.user_id = auth.user.id
    }
    
    // Save instance
    await device.save()

    const res = {
      success: true,
      device: device
    }

    // AUDIT RECORD
    await ElasticService.insertAuditRecord(request.method(), auth.user.id, device.id, `Device has been created at id: ${device.id}`)

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return response.redirect('/devices')
    }
  }

  /**
   * Display a single device.
   * GET devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ auth, params, request, response }) {
    const role = await Role.query().where({
      device_show: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role && auth.user.organization_id) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const device = await Device.query().where({
      id: params.id,
    }).first()

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    } else {
      if (device.user_id == auth.user.id || device.organization_id == auth.user.organization_id) {

        const device_data = await Redis.get(`cpu_stats:${params.id}`)
        const gps_data = await Redis.get(`gps:${params.id}`)
        
        device.info = JSON.parse(device_data)
        device.location = JSON.parse(gps_data)

        const res = {
          success: true,
          device: device
        }

        // AUDIT RECORD
        // await ElasticService.insertAuditRecord(auth.user.id, `Shown propety info for ${params.id}`)

        // Get the request URL
        const url = request.url()
        // Check if request from api route
        if (url.includes('/api/v1')) {
          return response.json(res)
        } else {
          return response.redirect('/devices')
        }

      } else {
        return response.status(401).json({
          success: false,
          message: `You are unable to access this resource, please contact your admin if this is incorrect`
        })
      }
    }
  }

  /**
   * Render a form to update an existing device.
   * GET devices/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update device details.
   * PUT or PATCH devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a device with id.
   * DELETE devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ auth, params, request, response }) {
    const role = await Role.query().where({
      device_destroy: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role && auth.user.organization_id) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const device = await Device.query().where({
      id: params.id,
    }).first()

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    } else {
      if (device.user_id == auth.user.id || device.organization_id == auth.user.organization_id) {
        // Delete instance
        await device.delete()
        // Show user success
        // session.flash({ notification: "Device deleted!" })

        // AUDIT RECORD
        await ElasticService.insertAuditRecord(request.method(), auth.user.id, params.id, `Device at ${params.id} has been deleted`)

        // Get the request URL
        const url = request.url()

        // Check if request from api route
        if (url.includes('/api/v1')) {
          return response.json({
            success: true,
            message: `Device ${params.id} deleted`
          })
        } else {
          // return to page
          return response.redirect('/posts')
        }
      } else {
        return response.status(401).json({
          success: false,
          message: `You are unable to access this resource, please contact your admin if this is incorrect`
        })
      }
    }
  }

  /**
   * Display a single device.
   * GET devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async history ({ auth, params, request, response, view }) {

    const role = await Role.query().where({
      device_history: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role && auth.user.organization_id) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    // Validate
    const validation = await validate(request.all(), {
      interval: 'required|string|min:2|max:5',
      field: 'required|string|min:3|max:255'
    })
    // Check if validation failed
    if(validation.fails()) {
      return response.json(validation.messages())
    }

    const device = await Device.query().where({
      id: params.id,
    }).first()

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    } else {
      if (device.user_id == auth.user.id || device.organization_id == auth.user.organization_id) {

        // AUDIT RECORD
        await ElasticService.insertAuditRecord(request.method(), auth.user.id, device.id,`Viewing historical info for ${params.id} at ${request.input('interval')} for ${request.input('field')}`)

        // callback API
        const my_q = await elasticClient.search({
          index: 'device-metrics*',
          body: {
            query: {
              bool: {
                must: [
                  {
                    match_phrase: {
                      device_id: params.id
                    }
                  }
                ]
              }
            },
            size: 0,
            aggregations: {
              myDateHistogram: {
                date_histogram: {
                  field: "timestamp",
                  fixed_interval: request.input('interval')
                },
                aggs: {
                  stats: {
                    stats: {
                      field: request.input('field')
                    }
                  }
                }
              }
            }
          }
        })

        const stats = my_q.body.aggregations.myDateHistogram.buckets;

        const dev = {};
        dev.id = params.id
        dev[request.input('field').replace('.', '_')] = stats


        const res = {
          success: true,
          device: dev
        }

        // Get the request URL
        const url = request.url()
        // Check if request from api route
        if (url.includes('/api/v1')) {
          return response.json(res)
        } else {
          return response.redirect('/devices')
        }

      } else {
        return response.status(401).json({
          success: false,
          message: `You are unable to access this resource, please contact your admin if this is incorrect`
        })
      }
    }
  }

  /**
   * Display a single device.
   * GET devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async track ({ auth, params, request, response }) {

    const role = await Role.query().where({
      device_track: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role && auth.user.organization_id) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const device = await Device.query().where({
      id: params.id,
    }).first()

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    } else {

      if (device.user_id == auth.user.id || device.organization_id == auth.user.organization_id) {
        // callback API
        const my_q = await elasticClient.search({
          index: 'gps-metrics*',
          body: {
            query: {
              bool: {
                must: [
                  {
                    match_phrase: {
                      device_id: params.id
                    }
                  }
                ]
              }
            },
            size: 2000,
            sort: [
              {
                timestamp: {
                  order: "desc"
                }
              }
            ],
          }
        })

        const stats = my_q.body.hits.hits

        const locations = []
        for (let i = 0; i < stats.length; i++) {
          locations.push(stats[i]._source)
        }

        const dev = {};
        dev.id = params.id
        dev['locations'] = locations


        const res = {
          success: true,
          device: dev
        }

        // AUDIT RECORD
        await ElasticService.insertAuditRecord(request.method(), auth.user.id, params.id, `Viewing latest ipinfo for ${params.id}`)

        // Get the request URL
        const url = request.url()
        // Check if request from api route
        if (url.includes('/api/v1')) {
          return response.json(res)
        } else {
          return response.redirect('/devices')
        }
        
      } else {
        return response.status(401).json({
          success: false,
          message: `You are unable to access this resource, please contact your admin if this is incorrect`
        })
      }
    }
  }

  /**
   * Create/save a new device.
   * POST devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async reboot ({ params, auth, request, response }) {

    const role_with_org = await Role.query().where({
      device_reboot: 1,
      organization_id: auth.user.organization_id,
      device_id: params.id
    }).first()

    if (auth.user.organization_id && !role_with_org) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const device_with_user = await Device.query().where({
      id: params.id,
      user_id: auth.user.id
    }).with('user').first()

    const device_with_org = await Device.query().where({
      id: params.id,
      organization_id: auth.user.organization_id
    }).with('organization').first()

    const device = (auth.user.organization_id) ? device_with_org : device_with_user

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    }

    const msg = {
      command: "echo 'pi_user' | sudo -S reboot"
    }

    await mqttClient.publish(`command/${params.id}`, JSON.stringify(msg))

    const res = {
      success: true,
      message: `Device ${params.id} has been rebooted`
    }

    // AUDIT RECORD
    await ElasticService.insertAuditRecord(
      request.method(),
      auth.user.id,
      params.id,
      res['message']
    )

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return response.redirect('/devices')
    }
  }

  /**
   * Create/save a new device.
   * POST devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async connect ({ params, auth, request, response }) {

    const role_with_org = await Role.query().where({
      device_reboot: 1,
      organization_id: auth.user.organization_id,
      device_id: params.id
    }).first()

    if (auth.user.organization_id && !role_with_org) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const device_with_user = await Device.query().where({
      id: params.id,
      user_id: auth.user.id
    }).with('user').first()

    const device_with_org = await Device.query().where({
      id: params.id,
      organization_id: auth.user.organization_id
    }).with('organization').first()

    const device = (auth.user.organization_id) ? device_with_org : device_with_user

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    }

    const msg = {
      command: "Establish ngrok connection"
    }

    await mqttClient.publish(`connect/${params.id}`, JSON.stringify(msg))

    const res = {
      success: true,
      message: `Device ${params.id} monitoring has been started`
    }

    // AUDIT RECORD
    await ElasticService.insertAuditRecord(
      request.method(),
      auth.user.id,
      params.id,
      res['message']
    )

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return response.redirect('/devices')
    }
  }

  /**
   * Create/save a new device.
   * POST devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async command ({ params, auth, request, response }) {

    const role_with_org = await Role.query().where({
      device_command: 1,
      organization_id: auth.user.organization_id,
      device_id: params.id
    }).first()

    if (auth.user.organization_id && !role_with_org) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    // Validate
    const validation = await validate(request.all(), {
      command: 'required|string|min:2|max:255'
    })
    // Check if validation failed
    if(validation.fails()) {
      return response.json(validation.messages())
    }

    const device_with_user = await Device.query().where({
      id: params.id,
      user_id: auth.user.id
    }).with('user').first()

    const device_with_org = await Device.query().where({
      id: params.id,
      organization_id: auth.user.organization_id
    }).with('organization').first()

    const device = (auth.user.organization_id) ? device_with_org : device_with_user

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    }

    const msg = {
      command: request.input('command')
    }

    await mqttClient.publish(`command/${params.id}`, JSON.stringify(msg))

    const device_data = await Redis.get(`response:command:${params.id}`)

    const res = {
      success: true,
      message: `Ran '${request.input('command')}' on device ${params.id}`,
      data: device_data,
    }

    // AUDIT RECORD
    await ElasticService.insertAuditRecord(request.method(), auth.user.id, device.id, res['message'])

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return response.redirect('/devices')
    }
  }

  /**
   * Create/save a new device.
   * POST devices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async logs ({ params, auth, request, response }) {
     
    const role_with_org = await Role.query().where({
      device_logs: 1,
      organization_id: auth.user.organization_id,
      device_id: params.id
    }).first()

    if (auth.user.organization_id && !role_with_org) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const device_with_user = await Device.query().where({
      id: params.id,
      user_id: auth.user.id
    }).with('user').first()

    const device_with_org = await Device.query().where({
      id: params.id,
      organization_id: auth.user.organization_id
    }).with('organization').first()

    const device = (auth.user.organization_id) ? device_with_org : device_with_user

    if (!device) {
      return response.status(404).json({
        success: false,
        message: `Device ${params.id} does not exist`
      })
    }

    // callback API
    const my_q = await elasticClient.search({
      index: 'audit-logs*',
      body: {
        query: {
          bool: {
            must: [
              {
                match_phrase: {
                  device_id: params.id
                }
              }
            ]
          }
        },
        size: 100,
        sort: [
          {
            timestamp: {
              order: "desc"
            }
          }
        ],
      }
    })

    const stats = my_q.body.hits.hits

    const logs = []
    for (let i = 0; i < stats.length; i++) {
      logs.push(stats[i]._source)
    }

    const dev = {};
    dev.id = params.id
    dev['logs'] = logs

    const res = {
      success: true,
      device: dev
    }

    // AUDIT RECORD
    await ElasticService.insertAuditRecord(request.method(), auth.user.id, params.id, `Viewing Logs for ${params.id}`)

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return response.redirect('/devices')
    }
  }
}

module.exports = DeviceController
