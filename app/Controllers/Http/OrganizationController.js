'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

// Models
const Organization = use('App/Models/Organization')
const User = use('App/Models/User')
const Role = use('App/Models/Role')

// Validator
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with organizations
 */
class OrganizationController {
  /**
   * Show a list of all organizations.
   * GET organizations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const orgs = await Organization.all()
    return response.json({
      success: true,
      organizations: orgs.toJSON()
    })
  }

  /**
   * Render a form to be used for creating a new organization.
   * GET organizations/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new organization.
   * POST organizations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store({ auth, request, response, session }) {

    if (auth.user.organization_id) {
      return response.status(409).json({
        success: false,
        error: "User already belongs to an organization"
      })
    }

    // Validate
    const validation = await validate(request.all(), {
      name: 'required|min:3|max:255',
      email: 'required|min:3',
      website: 'string'
    })
    // Check if validation failed
    if(validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }

    // try {
      // Create new instance
      const organization = new Organization();
      // Assign detials
      organization.name = request.input('name')
      organization.email = request.input('email')
      organization.website = request.input('website')
      await organization.save()

      // Update User
      const auth_user = await User.find(auth.user.id)
      auth_user.organization_id = organization.id
      await auth_user.save()
    
      // Add role
      const main_role = new Role();
      main_role.organization_id = organization.id
      main_role.user_id = auth.user.id
      
      main_role.role_index = 1
      main_role.role_store = 1
      main_role.role_show = 1
      main_role.role_update = 1
      main_role.role_destroy = 1

      main_role.organization_index = 1
      main_role.organization_store = 1
      main_role.organization_show = 1
      main_role.organization_update = 1
      main_role.organization_destroy = 1
      await main_role.save()

    // } catch(e) {
    //   return response.json({
    //     success: false,
    //     error: e
    //   })
    // }

    // Show user success
    session.flash({ notification: "Org created!" })
    // return to page
    return response.json({
      success: true,
      organization: organization
    })
  }

  /**
   * Display a single organization.
   * GET organizations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ auth, params, request, response, view }) {
    const role = await Role.query().where({
      organization_show: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const org = await Organization.query().where({
      id: auth.user.organization_id   
    }).where({
      id: params.id 
    }).with([
      'devices.user'
    ]).first()

     return response.json({
      success: true,
      organization: org
    })
  }

  /**
   * Render a form to update an existing organization.
   * GET organizations/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update organization details.
   * PUT or PATCH organizations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ auth, params, request, response, session }) {
    const role = await Role.query().where({
      organization_update: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    // Validate
    const validation = await validate(request.all(), {
      name: 'required|min:3|max:255',
      email: 'required||email|unique:organizations',
      website: 'url|string',
    })
    // Check if validation failed
    if(validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }
    // Create new instance
    const org = await Organization.find(params.id)

    // Assign detials
    org.name = request.input('name')
    org.email = request.input('email')
    org.website = request.input('website')

    // Save instance
    await org.save()
    // Show user success
    session.flash({ notification: "Organization updated!" })
    // return to page
    return response.json({
      success: true,
      organization: org
    })
  }

  /**
   * Delete a organization with id.
   * DELETE organizations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async destroy({ auth, params, session, response }) {
    const role = await Role.query().where({
      organization_destroy: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

      // Find instance
    const org = await Organization.find(params.id)
    // Delete instance
    await org.delete()
    // Show user success
    session.flash({ notification: "Organization deleted!" })
    // return to page
    return response.json({
      success: true,
      message: 'Organization has been deleted'
    })
  }
}

module.exports = OrganizationController
