'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Role = use('App/Models/Role')

const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with roles
 */
class RoleController {
  /**
   * Show a list of all roles.
   * GET roles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ auth, request, response, view }) {
    const role = await Role.query().where({
      role_index: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const roles = await Role.all()
    return response.json({
      success: true,
      roles: roles.toJSON()
    })
  }

  /**
   * Render a form to be used for creating a new role.
   * GET roles/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new role.
   * POST roles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const role_user = await Role.query().where({
      role_store: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role_user) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    try {
      const role = new Role()
      role.user_id = request.input('user_id')
      role.organization_id = auth.user.organization_id
      role.device_index = request.input('device_index')
      role.device_store = request.input('device_store')
      role.device_show = request.input('device_show')
      role.device_destroy = request.input('device_destroy')
      role.device_history = request.input('device_history')
      role.device_track = request.input('device_track')
      // rols
      role.role_index = request.input('role_index')
      role.role_store = request.input('role_store')
      role.role_show = request.input('role_show')
      role.role_destroy = request.input('role_destroy')
      // orgs
      role.organization_index = request.input('organization_index')
      role.organization_store = request.input('organization_store')
      role.organization_show = request.input('organization_show')
      role.organization_update = request.input('organization_update')
      role.organization_destroy = request.input('organization_destroy')
      await role.save()
    } catch(e) {
      if (e.errno == 1062) {
        return response.status(409).json({
          success: false,
          error: "Duplicate record found"
        })
      } else {
        return response.status(500).json({
          success: false,
          error: e
        })
      }
    }

    return role
  }

  /**
   * Display a single role.
   * GET roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ auth, params, request, response, view }) {
    const role_user = await Role.query().where({
      role_show: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role_user) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }
  }

  /**
   * Render a form to update an existing role.
   * GET roles/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update role details.
   * PUT or PATCH roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ auth, params, request, response }) {
    const role_user = await Role.query().where({
      role_update: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role_user) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }

    const role = await Role.find(params.id)

    if (!role) {
      return response.status(404).json({
        success: false,
        message: `Role ${params.id} does not exist`
      })
    }
    
    role.device_index = request.input('device_index')
    role.device_store = request.input('device_store')
    role.device_show = request.input('device_show')
    role.device_destroy = request.input('device_destroy')
    role.device_history = request.input('device_history')
    role.device_track = request.input('device_track')
    role.device_reboot = request.input('device_reboot')
    role.device_command = request.input('device_command')
    role.device_logs = request.input('device_logs')
    // rols
    role.role_index = request.input('role_index')
    role.role_store = request.input('role_store')
    role.role_show = request.input('role_show')
    role.role_update = request.input('role_update')
    role.role_destroy = request.input('role_destroy')
    // orgs
    role.organization_index = request.input('organization_index')
    role.organization_store = request.input('organization_store')
    role.organization_show = request.input('organization_show')
    role.organization_update = request.input('organization_update')
    role.organization_destroy = request.input('organization_destroy')
    await role.save()

    return role
  }

  /**
   * Delete a role with id.
   * DELETE roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ auth, params, request, response }) {
    const role_user = await Role.query().where({
      role_destroy: 1,
      organization_id: auth.user.organization_id,
      user_id: auth.user.id
    }).first()

    if (!role_user) {
      return response.status(401).json({
        success: false,
        message: `You are unable to access this resource, please contact your admin if this is incorrect`
      })
    }
  }
}

module.exports = RoleController
