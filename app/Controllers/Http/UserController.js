'use strict'

const Redis = use('Redis')
const User = use('App/Models/User')

class UserController {

  async redis ({ response }) {

    const val = await Redis.get('cpu_stats:7b8ff42d-1134-42c0-8daf-021dfca91d1f')
    return response.json(val);
  }

  /**
   * Display a single device.
   * GET devices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async authUser ({ auth, params, request, response, view }) {
    const user = await User.query().where({
      id: auth.user.id,
    }).with('organization').first()

    if (!user) {
      return response.status(404).json({
        success: false,
        message: `User ${params.id} does not exist`
      })
    }

    const res = {
      success: true,
      user: user
    }

    // Get the request URL
    const url = request.url()
    // Check if request from api route
    if (url.includes('/api/v1')) {
      return response.json(res)
    } else {
      return view('users.show')
    }
  }
}

module.exports = UserController
