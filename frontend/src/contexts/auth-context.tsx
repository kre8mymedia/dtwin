import React, { useContext, useMemo } from 'react';
import { createContext } from 'react';

import { AxiosError } from 'axios';
import { useHistory } from 'react-router-dom';

import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';

import userAPI from 'src/api/auth';
import { sleep } from 'src/utils/sleep';
import { IReactChildren } from 'src/types/react-children';
import store from 'src/state';
import { setCSRF } from 'src/state/user/actions';
import { signup as register } from 'src/api/auth';
interface IAuthContextProps {
  loading: boolean;
  user: boolean;
  csrf: null | string;
  loginErrors: {
    email: null | string;
    password: null | string;
    message: null | string;
  };
  login: (
    email: string,
    password: string,
  ) => { message: string } | Promise<void>;
  signup: (
    email: string,
    password: string,
  ) => { message: string } | Promise<void>;
  signupStep: number;
  setSignupStep: React.Dispatch<React.SetStateAction<number>>;
  logout: () => void;
}

const AuthContext = createContext({} as IAuthContextProps);

export function AuthProvider({ children }: IReactChildren) {
  const [loading, setLoading] = React.useState(false);
  const [isCheckingSession, setIsCheckingSession] = React.useState(true);
  const [signupStep, setSignupStep] = React.useState(0);
  const [user, setUser] = React.useState(false);
  const [csrf, setCsrf] = React.useState<string | null>(null);
  const [loginErrors, setLoginErrors] = React.useState<{
    email: null | string;
    password: null | string;
    message: null | string;
  }>({
    email: null,
    password: null,
    message: null,
  });

  const history = useHistory();

  React.useEffect(() => {
    const fetchCsrfToken = async () => {
      const result = await fetch('http://localhost:8000/api/v1/user/csrf', {
        credentials: 'include',
      });

      const csrfToken = result.headers.get('X-CSRFToken');

      setCsrf(csrfToken);
      store.dispatch(setCSRF(csrfToken));
    };

    fetchCsrfToken();
  }, []);

  React.useEffect(() => {
    const checkSession = async () => {
      await sleep(3000);
      try {
        const r = await userAPI.checkSession();
        console.log('check session: ', r);
        setUser(r.data.isAuthenticated);
      } catch (error) {
        setUser(false);
      } finally {
        setIsCheckingSession(false);
      }
    };

    checkSession();
  }, []);

  const login = async (email: string, password: string) => {
    setLoading(true);
    await sleep(3000);
    try {
      const r = await userAPI.login({ email, password });
      console.log(r);
      setUser(true);
      history.replace('/dashboard');
      setLoading(false);
    } catch (e) {
      const error = e as AxiosError;

      if (error.response) {
        setLoginErrors({ ...loginErrors, message: error.response.data.detail });
      }
      console.log(error);
      setLoading(false);
    }
  };

  const signup = async (email: string, password: string) => {
    setLoading(true);
    await sleep(3000);
    try {
      await register(email, password);
      // history.replace('/login');
      setSignupStep(1);
      setLoading(false);
    } catch (e) {
      const error = e as AxiosError;

      if (error.response) {
        setLoginErrors({ ...loginErrors, message: error.response.data.detail });
      }
      console.log(error);
      setSignupStep(0);
      setLoading(false);
    }
  };

  const logout = async () => {
    try {
      await userAPI.logout();
      window.location.replace('/login');
    } catch (error) {
      console.log(error);
      document.cookie =
        'sessionid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
      history.replace('/login');
    }
  };

  // Make the provider update only when it should.
  // We only want to force re-renders if the user,
  // loading or error states change.
  //
  // Whenever the `value` passed into a provider changes,
  // the whole tree under the provider re-renders, and
  // that can be very costly! Even in this case, where
  // you only get re-renders when logging in and out
  // we want to keep things very performant.
  const memoedValue = useMemo(
    () => ({
      user,
      loading,
      signupStep,
      csrf,
      loginErrors,
      signup,
      login,
      logout,
      setSignupStep,
    }),
    [user, loading, signupStep],
  );

  function renderContent() {
    if (isCheckingSession) {
      return (
        <div style={{ height: '100vh' }}>
          <Container
            maxWidth="sm"
            style={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <CircularProgress />
          </Container>
        </div>
      );
    }

    return children;
  }

  return (
    <AuthContext.Provider value={memoedValue}>
      <div
        style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}
      >
        {renderContent()}
      </div>
    </AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}

export { AuthContext };
