import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import { AuthProvider } from 'src/contexts/auth-context';
import DashboardPage from '../pages/dashboard';
import LandingPage from '../pages/landing';
import LoginPage from '../pages/login';
import SignupPage from '../pages/signup';
import TodosPage from '../pages/todos';
import PrivateRoute from './private-route';
import PublicRoute from './public-route';

export default function AppRouter() {
  return (
    <Router>
      <AuthProvider>
        <Switch>
          <PublicRoute exact path="/">
            <LandingPage />
          </PublicRoute>
          <PublicRoute path="/login">
            <LoginPage />
          </PublicRoute>
          <PublicRoute path="/signup">
            <SignupPage />
          </PublicRoute>
          <PrivateRoute path="/dashboard">
            <DashboardPage />
          </PrivateRoute>
          <PrivateRoute path="/todos">
            <TodosPage />
          </PrivateRoute>
        </Switch>
      </AuthProvider>
    </Router>
  );
}
