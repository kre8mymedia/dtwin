import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import { useAuth } from 'src/contexts/auth-context';
import PrivateLayout from '../layouts/private-layout';

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
export default function PrivateRoute({ children, ...rest }: RouteProps) {
  const { user } = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        user ? (
          <PrivateLayout>{children}</PrivateLayout>
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
