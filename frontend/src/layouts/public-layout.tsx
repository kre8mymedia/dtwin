import React from 'react';

import { Link as RouterLink } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    color: '#000',
  },
  appBar: {
    backgroundColor: 'transparent',
  },
}));

export default function PublicLayout({
  children,
}: Partial<React.PropsWithChildren<JSX.Element>>) {
  const classes = useStyles();
  return (
    <>
      <AppBar position="static" className={classes.appBar} elevation={0}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link component={RouterLink} to="/">
              MyApp
            </Link>
          </Typography>
          <Button color="primary" component={RouterLink} to="/signup">
            Signup
          </Button>
          <Button color="primary" component={RouterLink} to="/login">
            Login
          </Button>
        </Toolbar>
      </AppBar>
      <div style={{ display: 'flex', flex: 1, marginTop: -64 }}>
        <Container
          maxWidth="sm"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
        >
          <>{children}</>
        </Container>
      </div>
    </>
  );
}
