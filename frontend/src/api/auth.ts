import httpClient from 'src/http/client';
import store, { RootState } from 'src/state';

const client = httpClient();

let csrf: string | null = '';

const updateCsrf = () => {
  const state: RootState = store.getState();
  csrf = state.session.csrf;
};
store.subscribe(updateCsrf);

export const signup = async (email: string, password: string) => {
  return client.post(
    '/user/register',
    {
      email: email,
      password: password,
    },
    {
      headers: {
        'X-CSRFToken': csrf,
      },
      withCredentials: true,
    },
  );
};

const user = {
  login: async (params: { email: string; password: string }) => {
    const r = await client.post(
      '/user/login',
      {
        email: params.email,
        password: params.password,
      },
      {
        headers: {
          'X-CSRFToken': csrf,
        },
        withCredentials: true,
      },
    );
    console.log(r);
  },
  checkSession: () => {
    return client.get('/user/session', {
      withCredentials: true,
    });
  },
  logout: () => {
    return client.get('/user/logout', {
      withCredentials: true,
    });
  },
};

export default user;
