import { AxiosInstance } from 'axios';
// import httpClient from 'src/http/client';

export default function todos(client: AxiosInstance) {
  // const client = httpClient();
  return {
    login: async (params: { email: string; password: string }) => {
      const r = await client.post('/user/login', {
        data: params,
      });
      console.log(r);
    },
  };
}
