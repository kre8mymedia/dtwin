import axios from 'axios';

export default function httpClient() {
  const instance = axios.create({
    baseURL: 'http://localhost:8000/api/v1',
    timeout: 10000,
    xsrfHeaderName: 'X-CSRFToken',
    withCredentials: true,
  });

  return instance;
}
