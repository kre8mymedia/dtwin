'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

// Factory.blueprint('App/Models/User', (faker) => {
//   return {
//     username: faker.username()
//   }
// })

Factory.blueprint('App/Models/Device', async (faker) => {
  return {
    slug: faker.hash({length: 8}),
    user_id: 1
  }
})

Factory.blueprint('App/Models/Role', async (faker) => {
  return {
    user_id: 1,
    organization_id: 1,
    device_index: 1,
    device_store: 1,
    device_show: 1,
    device_destroy: 1,
    device_history: 1,
    device_track: 0,
    role_index: 1,
    role_store: 2,
    role_show: 1,
    role_destroy: 0,
    organization_index: 0,
    organization_store: 0,
    organization_show: 0,
    organization_update: 0,
    organization_destroy: 0
  }
})