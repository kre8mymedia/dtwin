'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DeviceSchema extends Schema {
  up() {
    this.create('devices', (table) => {
      table.uuid("id").primary()
      table.string('slug')
      // Relationships
      table.uuid('organization_id').index()
      table.uuid('user_id').index()
      table.timestamps()
    })
  }

  down() {
    this.drop('devices')
  }
}

module.exports = DeviceSchema
