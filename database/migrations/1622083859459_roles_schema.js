'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolesSchema extends Schema {
  up () {
    this.create('roles', (table) => {
      table.uuid("id").primary()
      table.timestamps()
      // Relationships
      table.uuid('user_id').index();
      table.uuid('organization_id').index();
      table.uuid('device_id').index().default(0);
      table.unique(['user_id', 'organization_id', 'device_id'])
      // Devices
      table.boolean('device_index').default(0)
      table.boolean('device_store').default(0)
      table.boolean('device_show').default(0)
      table.boolean('device_destroy').default(0)
      table.boolean('device_history').default(0)
      table.boolean('device_track').default(0)
      table.boolean('device_reboot').default(0)
      table.boolean('device_command').default(0)
      table.boolean('device_logs').default(0)
      // Roles
      table.boolean('role_index').default(0)
      table.boolean('role_store').default(0)
      table.boolean('role_show').default(0)
      table.boolean('role_update').default(0)
      table.boolean('role_destroy').default(0)
      // Organization
      table.boolean('organization_index').default(0)
      table.boolean('organization_store').default(0)
      table.boolean('organization_show').default(0)
      table.boolean('organization_update').default(0)
      table.boolean('organization_destroy').default(0)
    })
  }

  down () {
    this.drop('roles')
  }
}

module.exports = RolesSchema
