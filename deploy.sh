#!/bin/bash

# helm -n adonis-$1 install --set imageRepo=$2 adonis-$1 ./myHelm/prod

helm -n adonis-$1 install --set imageRepo=$2 ingest-$1-cpu ./myHelm/ingest

helm -n adonis-$1 install --set imageRepo=$3 ingest-$1-gps ./myHelm/gps

echo "Waiting for External-IP..."
sleep 1
echo "Waiting for External-IP..."
sleep 1
echo "Waiting for External-IP..."
sleep 1
echo "Waiting for External-IP..."
kubectl -n adonis-$1 get all