#!/bin/bash

docker build -t "sk8er71091/adonis:$1" .
docker push "sk8er71091/adonis:$1"

# cd ./ingestors/cpu

# docker build -t "sk8er71091/cpu_ingestor:$1" .
# docker push "sk8er71091/cpu_ingestor:$1"

# cd ../gps

# docker build -t "sk8er71091/gps_ingestor:$1" .
# docker push "sk8er71091/gps_ingestor:$1"

echo ">> API: sk8er71091/adonis:$1"
# echo ">> CPU INGESTOR: sk8er71091/cpu_ingestor:$1"
# echo ">> GPS INGESTOR: sk8er71091/gps_ingestor:$1"